version: '3.5'
services:
    mongodb:
        image: mongo:3.6
        hostname: mongodb
        container_name: db-mongo
        ports:
            - '27017:27017'
        command: --bind_ip_all --smallfiles
        volumes:
            - mongodb:/data

    mysql-db:
        restart: always
        image: mysql:5.7
        hostname: mysql-db
        container_name: db-mysql
        expose:
            - '3306'
        ports:
            - '3306:3306'
        networks:
            - default
        environment:
            - 'MYSQL_ROOT_PASSWORD=123'
            - 'MYSQL_ROOT_HOST=%'
        volumes:
            - mysql-db:/var/lib/mysql

    draco:
        image: ging/fiware-draco:1.1.0
        hostname: draco
        container_name: fiware-draco
        networks:
            - default
        environment:
            - NIFI_WEB_HTTP_PORT=9090
        expose:
            - '5050'
            - '9090'
        ports:
            - '9090:9090'
            - '5050:5050'
        healthcheck:
            test: curl --fail -s http://draco:9090/nifi-api/system-diagnostics || exit 1
            start_period: 1m

    grafana:
        image: grafana/grafana
        container_name: grafana
        depends_on:
            - mysql-db
            - mongodb
            - orion
        ports:
            - '3000:3000'
        environment:
            - 'GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-worldmap-panel'
        volumes:
            - grafana:/var/lib/grafana

    orion:
        image: fiware/orion:2.2.0
        hostname: orion
        container_name: fiware-orion
        depends_on:
            - mongodb
        expose:
            - '1026'
        ports:
            - '1026:1026'
        command: -dbhost mongodb -logLevel DEBUG
        healthcheck:
            test: curl --fail -s http://orion:1026/version || exit 1

    iot-agent:
        image: fiware/iotagent-ul:1.8.0
        hostname: iot-agent
        container_name: fiware-iot-agent
        depends_on:
            - mongodb
        ports:
            - '4041:4041'
            - '7896:7896'
        environment:
            - IOTA_CB_HOST=orion # name of the context broker to update context
            - IOTA_CB_PORT=1026 # port the context broker listens on to update context
            - IOTA_NORTH_PORT=4041
            - IOTA_REGISTRY_TYPE=mongodb #Whether to hold IoT device info in memory or in a database
            - IOTA_LOG_LEVEL=DEBUG # The log level of the IoT Agent
            - IOTA_TIMESTAMP=true # Supply timestamp information with each measurement
            - IOTA_CB_NGSI_VERSION=v2 # use NGSIv2 when sending updates for active attributes
            - IOTA_AUTOCAST=true # Ensure Ultralight number values are read as numbers not strings
            - IOTA_MONGO_HOST=mongodb # The host name of MongoDB
            - IOTA_MONGO_PORT=27017 # The port mongoDB is listening on
            - IOTA_MONGO_DB=iotagentul # The name of the database used in mongoDB
            - IOTA_HTTP_PORT=7896 # The port used for device traffic over HTTP
            - IOTA_PROVIDER_URL=http://iot-agent:4041
        healthcheck:
            test: curl --fail -s http://iot-agent:4041/iot/about || exit 1

volumes:
    mongodb: ~
    grafana: ~
    mysql-db: ~
