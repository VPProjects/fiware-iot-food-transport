var socket = io();
var barChart;
var lineChart;
var map;

$('#inputMarket').change(() => {
	let selectedMaketId = $('#dropdownMarket').val();
	socket.emit('marketDashboard', selectedMaketId);
});

socket.on('renderDashboard', (barChartData, mapData, lineChartData) => {
	updateBarChart(barChartData.labels, barChartData.data, barChartData.title);
	console.log(JSON.stringify(lineChartData));
	updateLineChart(lineChartData.lineChartLabels, lineChartData.datasets, lineChartData.title);
	updateMap(mapData.coordinates, mapData.title, mapData.marketName, mapData.marketAddress);
});

socket.on('testing', (v) => alert(v));

function updateLineChart(labels, datasets, title) {
	$('#lineChartTitle').text(title);
	lineChart.data.labels = labels;
	lineChart.data.datasets = datasets;
	lineChart.update();
}

function updateBarChart(labels, data, chartTitle) {
	$('#barChartTitle').text(chartTitle);
	barChart.data.labels = labels;
	barChart.data.datasets[0].data = data;
	barChart.update();
}

function updateMap(coordinates, mapTitle, marketName, marketStreetAddress) {
	$('#mapTitle').text(mapTitle);
	map.setView(coordinates, 14);
	let marker = L.marker(coordinates).addTo(map);
	marker
		.bindPopup(`<b>Market: </b>${marketName}<br/><b>Address: </b>${marketStreetAddress}`, { maxWidth: 200 })
		.openPopup();
}

$(function() {
	var ctx = document.getElementById('barChart').getContext('2d');
	var lineCtx = document.getElementById('lineChart').getContext('2d');

	barChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
			labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
			datasets: [
				{
					label: 'Data',
					data: [12, 19, 3, 5, 2, 3],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)'
					],
					borderColor: [
						'rgba(255, 99, 132, 1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(255, 159, 64, 1)'
					],
					borderWidth: 1
				}
			]
		},
		options: {
			scales: {
				xAxes: [
					{
						ticks: {
							beginAtZero: true
						}
					}
				]
			}
		}
	});

	lineChart = new Chart(lineCtx, {
		type: 'line',
		data: {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [
				{
					label: 'My First dataset',
					backgroundColor: 'rgba(255, 99, 132, 0.2)',
					borderColor: 'rgba(255, 99, 132, 1)',
					data: [5, 13, 14, 10, 2],
					fill: false
				},
				{
					label: 'My Second dataset',
					backgroundColor: 'rgba(54, 162, 235, 0.2)',
					borderColor: 'rgba(54, 162, 235, 1)',
					data: [90, 9, 94, 8, 50, 4, 3],
					fill: false
				}
			]
		},
		options: {
			responsive: true,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [
					{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}
				],
				yAxes: [
					{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}
				]
			}
		}
	});

	map = L.map('mapid').setView([51.505, -0.09], 13);

	L.tileLayer(
		'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
		{
			maxZoom: 18,
			attribution:
				'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox.streets'
		}
	).addTo(map);
});
