const request = require('request');
const { updateMarketChart } = require('../sockets');

exports.healthCheck = (req, res) => {
	res.status(200).send('Food IoT is alive!');
};

exports.updateMarketChart = (req, res) => {
	updateMarketChart(req.body);
	res.status(204).send();
};
