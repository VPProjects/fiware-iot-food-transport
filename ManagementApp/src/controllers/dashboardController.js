const request = require('request');

exports.get = (req, res) => {
	const options = {
		url: 'http://localhost:1026/v2/entities/?type=Market,Farm,Field,Product&options=keyValues&attrs=id,name',
		method: 'GET'
	};

	const callback = (error, response, body) => {
		if (error) {
			console.log(error);
			return;
		}

		let dashboardData = JSON.parse(body);
		let markets = [];
		let farms = [];
		let fields = [];
		let products = [];

		dashboardData.forEach((element) => {
			switch (element.type) {
				case 'Market':
					markets.push(element);
					break;
				case 'Farm':
					farms.push(element);
					break;
				case 'Field':
					fields.push(element);
					break;
				case 'Product':
					products.push(element);
					break;
			}
		});
		res.render('dashboard', { title: 'Dashboard', markets, farms, fields, products });
	};

	request(options, callback);
};

exports.post = (req, res) => {};

exports.put = (req, res) => {};

exports.delete = (req, res) => {};
