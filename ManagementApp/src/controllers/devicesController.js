const request = require('request');

exports.index = (req, res) => {
	let headers = {
		'fiware-service': 'openiot',
		'fiware-servicepath': '/'
	};

	let options = {
		url: 'http://localhost:4041/iot/devices',
		method: 'GET',
		headers: headers
	};

	let callback = function(error, response, body) {
		if (error) {
			console.log(error);
			res.send('ERRO');
		} else {
			console.log(JSON.stringify(body));
			let jsonRes = JSON.parse(body);
			res.render('devices/index', { title: 'Devices', devices: jsonRes.devices });
		}
	};

	request(options, callback);
};

exports.new = (req, res) => {
	res.render('devices/add');
};

exports.saveNew = (req, res) => {
	let device = req.body;

	for (let i = 0; i < device.attributes.length; i++) {
		if (device.attributes[i].object_id == '') {
			device.attributes.splice(i, 1);
			i--;
		}
	}

	for (let i = 0; i < device.static_attributes.length; i++) {
		if (device.static_attributes[i].name == '') {
			device.static_attributes.splice(i, 1);
			i--;
		}
	}

	console.log(device);

	let headers = {
		'Content-Type': 'application/json',
		'fiware-service': 'openiot',
		'fiware-servicepath': '/'
	};

	let options = {
		url: 'http://localhost:4041/iot/devices',
		method: 'POST',
		headers: headers,
		body: `{ 
            "devices": [
                ${JSON.stringify(device)}
            ]
        }`
	};

	let callback = function(error, response, body) {
		if (error || response.statusCode != 201) {
			console.log(error + JSON.stringify(body));
			res.send('ERRO');
		} else {
			res.redirect('/devices');
		}
	};

	request(options, callback);
};

exports.put = (req, res) => {};

exports.delete = (req, res) => {};
