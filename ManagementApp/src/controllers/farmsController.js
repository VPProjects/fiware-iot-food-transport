exports.get = (req, res) => {
	res.render('farms', { title: 'Farms' });
};

exports.new = (req, res) => {
	res.render('farms/add');
};

exports.post = (req, res) => {};

exports.put = (req, res) => {};

exports.delete = (req, res) => {};
