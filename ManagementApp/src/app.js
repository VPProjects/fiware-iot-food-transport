const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
const { sockets } = require('./sockets');

app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

// Template Configs
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.set('views', __dirname + '/views');

// Rotas
const indexRouter = require('./routes/indexRouter');
const dashboardRouter = require('./routes/dashboardRouter');
const devicesRouter = require('./routes/devicesRouter');
const orionRouter = require('./routes/orionRouter');

app.use('/', indexRouter);
app.use('/dashboard', dashboardRouter);
app.use('/devices', devicesRouter);
app.use('/orion', orionRouter);

sockets(http);

module.exports = {
	http
};
