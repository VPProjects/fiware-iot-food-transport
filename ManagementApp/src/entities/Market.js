class Market{
	constructor(id, name, address, locationCoordinates, refDistributionCenter){
		
		this.type = "Market";
		this.id = `urn:nsgi-ld:Market:${id}`;
		this.name = name;
		this.address = {
			"type": "PostalAddress",
			"value": {
				"streetAddress": `${address[0]}`,
				"addressRegion": `${address[1]}`,
				"addressLocality": `${address[2]}`,
				"postalCode": `${address[3]}`
			}
		};
	}
}

exports.Market = Market;
