class Plantation{
	constructor(id, amount, startDate, 
							endDate, locationCoordinates, refCrop, 
							refField){
		
		this.type = "Plantation";
		this.id = `urn:nsgi-ld:Plantation:${id}`;
		this.amount = amount;
		this.startDate = startDate;
		this.endDate = endDate;
		this.location = {
			"type":"geo:json",
			"value": {
				"type": "Point",
				"coordinates": locationCoordinates
			}
		}

		this.refCrop = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Crop:${refCrop}`
		};

		this.refField = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Field:${refField}`
		};
	}
}
exports.Plantation = Plantation;
