class StockLight{
	constructor(id, state, refStock)

		this.type = "StockLight";
		this.id = `urn:nsgi-ld:StockLight:${id}`;
		this.state = state;
		this.refStock = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Stock:${refStock}`
		};
	}
}

exports.StockLight = StockLight;
