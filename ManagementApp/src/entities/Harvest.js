class Harvest{
	constructor(id, date, refPlantation){

		this.type = "Harvest";
		this.id = `urn:nsgi-ld:Harvest:${id}`;
		this.date = date;
		this.refPlantation = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Plantation:${refPlantation}`
		};
	}
}
exports.Harvest = Harvest;
