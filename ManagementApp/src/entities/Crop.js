class Crop{
	constructor(id, name, startDate, endDate, refProduct, refField){
	
		this.type = "Crop";
		this.id = `urn:nsgi-ld:Crop:${id}`;
		this.name = `${name}`;
		this.startDate = startDate;
		this.endDate = endDate;
		this.refProduct = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Product:${refProduct}`
		};
		this.refField = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Field:${refField}`
		};

	}

}

expots.Crop = Crop;
