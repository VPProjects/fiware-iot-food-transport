class Field{
	constructor(id, name, locationCoordinates, refFarm) {

		this.type = "Field";
		this.id = `urn:ngsi-ld:Field:${id}`;
		this.name = `${name}`;
		this.location = {
				"type": 'geo:json',
				"value": {
					"type": 'Point',
					"coordinates": locationCoordinates
				}
			};

		this.referenceFarm = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Farm:${refFarm}`
		};
	}
}

exports.Field = Field;
