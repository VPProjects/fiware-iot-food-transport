class Truck{
	constructor(id, brand, model, 
							locationCoordinates, refProduct, refFarm, 
							refDistributionCenter, refMarket){

	
		this.type = "Truck";
		this.id = `urn:nsgi-ld:Truck:${id}`;
		this.brand = brand;
		this.model = model;
		this.location = {
				"type": 'geo:json',
				"value": {
					"type": 'Point',
					"coordinates": locationCoordinates
				}
			};

		this.refProduct = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Product:${refProduct}`
		};

		this.refFarm = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Farm:${refFarm}`
		};

		this.refDistributionCenter = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:DistributionCenter:${refDistributionCenter}`
		};

		this.refMarket = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Market:${refMarket}`
		};

	}
}

exports.Truck = Truck;
