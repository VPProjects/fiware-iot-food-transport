class WeightSensor{
	constructor(id, measeure, refDistributionCenter){
		
		this.id = `urn:nsgi-ld:WeightSensor:${id}`;
		this.measeure = measeure;
		this.refDistributionCenter = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:DistributionCenter:${refDistributionCenter}`
		};
	}
}

exports.WeightSensor = WeightSensor;
