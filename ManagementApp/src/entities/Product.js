class Product{
	constructor(id, name) {

		this.type = "Product";
		this.id = `urn:nsgi-ld:Product:${id}`;
		this.name = `${name}`;

	}
}

exports.Product = Product;
