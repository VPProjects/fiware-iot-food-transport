class Stock{
	constructor(id, amount, refProduct, refMarket){

		this.type = "Stock";
		this.id = `urn:nsgi-ld:Stock:${id}`;
		this.amount = amount;
		this.refProduct = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Product:${refProduct}`
		};
		this.refMarket = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Market:${refMarket}`
		};
	}
}
exports.Stock = Stock;

