class TruckSensor{
	constructor(id, state, refTruck){

		this.type = "TruckSensor";
		this.id = `urn:nsgi-ld:TruckSensor:${id}`;
		this.state = state;
		this.refTruck = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:Truck:${refTruck}`
		};
	}
}
exports.TruckSensor = TruckSensor;
