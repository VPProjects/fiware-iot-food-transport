class Door{
	constructor(id, state, refDistributionCenter){
			
		this.id = `urn:nsgi-ld:Door:${id}`;
		this.state = state;
		this.refDistributionCenter = {
			"type": "Relationship",
			"value": `urn:nsgi-ld:DistributionCenter:${refDistributionCenter}`
		};
	}

}

exports.Door = Door;
