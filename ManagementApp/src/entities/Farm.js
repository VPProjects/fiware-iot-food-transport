class Farm{
	constructor(id, name, locationCoordinates) {
		
		this.type = "Farm";
		this.id = `urn:nsgi-ld:Farm:${id}`;
		this.name = `${name}`;
		this.location = {
			"type":"geo:json",
			"value": {
				"type": "Point",
				"coordinates": locationCoordinates
			}
		}
	}
}


exports.Farm = Farm; 
