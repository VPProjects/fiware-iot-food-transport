class DistributionCenter {
	constructor(id, name, locationCoordinates, refFarm) {
		this.type = 'DistributionCenter';
		this.id = `urn:nsgi-ld:DistributionCenter:${id}`;
		this.name = name;
		this.location = {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: locationCoordinates
			}
		};
		this.refFarm = {
			type: 'Relationship',
			value: `urn:nsgi-ld:Farm:${refFarm}`
		};
	}
}

exports.DistributionCenter = DistributionCenter;
