const request = require('request');
const socket = require('socket.io');
let _socket;
const backgroundColorArray = [
	'rgba(255, 99, 132, 0.2)',
	'rgba(54, 162, 235, 0.2)',
	'rgba(255, 206, 86, 0.2)',
	'rgba(75, 192, 192, 0.2)',
	'rgba(153, 102, 255, 0.2)',
	'rgba(255, 159, 64, 0.2)'
];
const borderColorArray = [
	'rgba(255, 99, 132, 1)',
	'rgba(54, 162, 235, 1)',
	'rgba(255, 206, 86, 1)',
	'rgba(75, 192, 192, 1)',
	'rgba(153, 102, 255, 1)',
	'rgba(255, 159, 64, 1)'
];

exports.sockets = (http) => {
	_http = http;
	io = socket(http);
	io.on('connection', (socket) => {
		_socket = socket;

		console.log('New Connection!');
		socket.on('disconnect', () => {
			console.log('Connection down');
		});

		socket.on('marketDashboard', (marketId) => {
			updateMarketDashboard(marketId);
		});
	});
};

exports.updateMarketChart = (market) => {
	updateMarketDashboard(market.id);
};

function updateMarketDashboard(marketId) {
	request(
		{
			url: 'http://localhost:1026/v2/entities/?type=Stock&options=keyValues&attrs=refProduct,amount',
			method: 'GET'
		},
		(error, response, body) => {
			if (error) {
				console.log(error);
				return;
			}

			let stocks = JSON.parse(body);
			let labels = [];
			let data = [];
			let datasets = [];
			let lineChartLabels = [];

			request(
				{
					url: 'http://localhost:1026/v2/entities/?type=Market&options=keyValues&attrs=address,location,name',
					method: 'GET'
				},
				(error, response, body) => {
					if (error) {
						console.log(error);
						return;
					}

					let market = JSON.parse(body)[0];

					stocks.forEach((stock, index) => {
						request(
							{
								url: `http://localhost:1026/v2/entities/${stock.refProduct}?options=values&attrs=name`,
								method: 'GET'
							},
							(error, response, body) => {
								if (error) {
									console.log(error);
									return;
								}

								let productName = JSON.parse(body)[0];
								let stockName = stock.id.substr(12) + ' - ' + productName;

								labels.push(stockName);
								data.push(parseInt(stock.amount));

								request(
									{
										url: `http://localhost:8668/v2/entities/${stock.id}/attrs/amount`,
										method: 'GET'
									},
									(error, response, body) => {
										if (error) {
											console.log(error);
											return;
										}

										let stockHistory = JSON.parse(body);

										if (!stockHistory.error) {
											let currentDataset = {
												label: stockName,
												backgroundColor: backgroundColorArray[index],
												borderColor: borderColorArray[index],
												data: stockHistory.data.values,
												fill: false
											};

											datasets.forEach((dataset, idx) => {
												for (let i = 0; i < stockHistory.data.values.length; i++) {
													dataset.data.push(dataset.data[dataset.data.length - 1]);
												}

												for (let j = 0; j < dataset.data.length - 1; j++) {
													var value = dataset.data[j];

													if (value != 0) {
														currentDataset.data.unshift(0);
													}
												}
												// dataset.data.forEach((value) => {
												// 	if (value != 0) {
												// 		currentDataset.data.unshift(0);
												// 	}
												// });
											});

											datasets.push(currentDataset);
											lineChartLabels = [...lineChartLabels, ...stockHistory.data.index];
										}

										if (index === stocks.length - 1) {
											let barChartData = {
												labels,
												data,
												title: 'Market Stock'
											};

											let mapData = {
												coordinates: market.location.coordinates,
												title: 'Market Location',
												marketName: market.name,
												marketAddress: market.address.streetAddress
											};

											let lineChartData = null;

											if (datasets.length > 0) {
												lineChartData = {
													lineChartLabels,
													datasets,
													title: 'Stock Log'
												};
											}

											console.log(JSON.stringify(lineChartData));
											_socket.emit('renderDashboard', barChartData, mapData, lineChartData);
										}
									}
								);
							}
						);
					});
				}
			);
		}
	);
}
