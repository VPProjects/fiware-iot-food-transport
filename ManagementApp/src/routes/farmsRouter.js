const express = require('express');
const router = express.Router();
const controller = require('../controllers/farmsController');

router.get('/', controller.get);
router.get('/add', controller.new);
router.post('/add', controller.post);
router.put('/edit/:id', controller.put);
router.delete('/delete/:id', controller.delete);

module.exports = router;
