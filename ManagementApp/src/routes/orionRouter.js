const express = require('express');
const router = express.Router();
const controller = require('../controllers/orionController.js');

router.get('/', controller.healthCheck);
router.post('/update-market-chart', controller.updateMarketChart);

module.exports = router;
