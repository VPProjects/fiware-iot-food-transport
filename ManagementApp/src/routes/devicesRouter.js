const express = require('express');
const router = express.Router();
const controller = require('../controllers/devicesController');

router.get('/', controller.index);
router.get('/add', controller.new);
router.post('/add', controller.saveNew);
router.put('/edit/:id', controller.put);
router.delete('/delete/:id', controller.delete);

module.exports = router;
