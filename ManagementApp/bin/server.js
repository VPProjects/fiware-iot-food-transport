const { http } = require('../src/app');

const port = process.env.PORT || '8000';

http.listen(port, function() {
	console.log(`App listening on port ${port}`);
});
