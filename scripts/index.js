var request = require('request');

var headers = {
	'Content-Type': 'application/json',
	'fiware-service': 'openiot',
	'fiware-servicepath': '/'
};

var dataString = `{
 "services": [
   {     
     "apikey":      "47AE0B0543AAE7061ACC9EA38BFDB1214D472D9DC3E17556A46C1E7F37CB80E6",
     "cbroker":     "http://orion:1026",
     "entity_type": "Thing",
     "resource":    "/iot/d"
   }
 ]
}`;

var options = {
	url: 'http://localhost:4041/iot/services',
	method: 'POST',
	headers: headers,
	body: dataString
};

function callback(error, response, body) {
	if (!error) {
		console.log(body);
	} else {
		console.log('Erro: ' + error);
	}
}

request(options, callback);
