var request = require('request');

var headers = {
	'Content-Type': 'application/json'
};

var dataString = {
	description: 'Notify stock changes',
	subject: {
		entities: [{ idPattern: '.*', type: 'Stock' }],
		condition: {
			attrs: ['amount']
		}
	},
	notification: {
		http: {
			url: 'http://quantumleap:8668/v2/notify'
		},
		attrs: ['amount']
	}
};

var options = {
	url: 'http://localhost:1026/v2/subscriptions',
	method: 'POST',
	headers: headers,
	body: JSON.stringify(dataString)
};

function callback(error, response, body) {
	if (!error) {
		console.log(body);
	} else {
		console.log('Erro: ' + error);
	}
}

request(options, callback);
