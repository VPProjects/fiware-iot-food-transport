var request = require('request');

let headers = {
	'Content-Type': 'application/json'
};

let farmsArray = [
	{
		id: 'urn:ngsi-ld:Farm:001',
		type: 'Farm',
		name: {
			type: 'Text',
			value: 'Fazenda FEI'
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.7242412, -46.5793979]
			}
		}
	}
];

let fieldsArray = [
	{
		id: 'urn:ngsi-ld:Field:001',
		type: 'Field',
		name: {
			type: 'Text',
			value: 'Capela'
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.7262486, -46.5802219]
			}
		},
		refFarm: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Farm:001'
		}
	},
	{
		id: 'urn:ngsi-ld:Field:002',
		type: 'Field',
		name: {
			type: 'Text',
			value: 'Prédio K'
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.725195, -46.58186]
			}
		},
		refFarm: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Farm:001'
		}
	}
];

let productsArray = [
	{
		id: 'urn:ngsi-ld:Product:001',
		type: 'Product',
		name: {
			type: 'Text',
			value: 'Corn'
		}
	},
	{
		id: 'urn:ngsi-ld:Product:002',
		type: 'Product',
		name: {
			type: 'Text',
			value: 'Bean'
		}
	},
	{
		id: 'urn:ngsi-ld:Product:003',
		type: 'Product',
		name: {
			type: 'Text',
			value: 'Grape'
		}
	}
];

let cropArray = [
	{
		id: 'urn:ngsi-ld:Crop:001',
		type: 'Crop',
		name: {
			type: 'Text',
			value: 'Corn 2019'
		},
		startDate: {
			type: 'Date',
			value: '2019-01-01'
		},
		endDate: {
			type: 'Date',
			value: '2019-12-31'
		},
		refProduct: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Product:001'
		},
		refField: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Field:001'
		}
	},
	{
		id: 'urn:ngsi-ld:Crop:002',
		type: 'Crop',
		name: {
			type: 'Text',
			value: 'Bean 2019'
		},
		startDate: {
			type: 'Date',
			value: '2019-01-01'
		},
		endDate: {
			type: 'Date',
			value: '2019-12-31'
		},
		refProduct: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Product:002'
		},
		refField: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Field:001'
		}
	},
	{
		id: 'urn:ngsi-ld:Crop:003',
		type: 'Crop',
		name: {
			type: 'Text',
			value: 'Grape 2019'
		},
		startDate: {
			type: 'Date',
			value: '2019-01-01'
		},
		endDate: {
			type: 'Date',
			value: '2019-12-31'
		},
		refProduct: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Product:003'
		},
		refField: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Field:002'
		}
	}
];

let plantationArray = [
	{
		id: 'urn:ngsi-ld:Plantation:001',
		type: 'Plantation',
		amount: {
			type: 'Number',
			value: '300'
		},
		startDate: {
			type: 'Date',
			value: '2019-01-10'
		},
		endDate: {
			type: 'Date',
			value: '2019-06-10'
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.725195, -46.58186]
			}
		},
		refCrop: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Crop:003'
		},
		refField: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Field:002'
		}
	}
];

let harvestsArray = [
	{
		id: 'urn:ngsi-ld:Harvest:001',
		type: 'Harvest',
		date: {
			type: 'Date',
			value: '2019-04-01'
		},
		amount: {
			type: 'Number',
			value: '90'
		},
		refPlantation: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Plantation:001'
		}
	},
	{
		id: 'urn:ngsi-ld:Harvest:002',
		type: 'Harvest',
		date: {
			type: 'Date',
			value: '2019-04-05'
		},
		amount: {
			type: 'Number',
			value: '150'
		},
		refPlantation: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Plantation:001'
		}
	}
];

let distributionCentersArray = [
	{
		id: 'urn:ngsi-ld:DistributionCenter:001',
		type: 'DistributionCenter',
		name: {
			type: 'Text',
			value: 'Mascarenhas'
		},
		address: {
			type: 'PostalAddress',
			value: {
				streetAddress: 'Rua José Mascarenhas 954',
				addressRegion: 'São Paulo',
				addressLocality: 'Vila Matilde',
				postalCode: '03515000'
			}
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.538274, -46.5302341]
			}
		}
	},
	{
		id: 'urn:ngsi-ld:DistributionCenter:002',
		type: 'DistributionCenter',
		name: {
			type: 'Text',
			value: 'Rio Real'
		},
		address: {
			type: 'PostalAddress',
			value: {
				streetAddress: 'Rua Rio Real 95',
				addressRegion: 'São Paulo',
				addressLocality: 'Vila Formosa',
				postalCode: '03358100'
			}
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.5668878, 46.5510129]
			}
		}
	}
];

let marketArray = [
	{
		id: 'urn:ngsi-ld:Market:001',
		type: 'Market',
		name: {
			type: 'Text',
			value: 'Dom Jaime HortiFruti'
		},
		address: {
			type: 'PostalAddress',
			value: {
				streetAddress: 'Av. Dom Jaime de Barros Câmara 620',
				addressRegion: 'São Bernardo do Campo',
				addressLocality: 'Planalto',
				postalCode: '09895400'
			}
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.660703, -46.583745]
			}
		},
		refDistributionCenter: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:DistributionCenter:001'
		}
	}
];

let stockArray = [
	{
		id: 'urn:ngsi-ld:Stock:001',
		type: 'Stock',
		amount: {
			type: 'Number',
			value: '400'
		},
		refProduct: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Product:001'
		},
		refMarket: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Market:001'
		}
	},
	{
		id: 'urn:ngsi-ld:Stock:002',
		type: 'Stock',
		amount: {
			type: 'Number',
			value: '50'
		},
		refProduct: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Product:003'
		},
		refMarket: {
			type: 'Relationship',
			value: 'urn:ngsi-ld:Market:001'
		}
	}
];

let trucksArray = [
	{
		id: 'urn:ngsi-ld:Truck:001',
		type: 'Truck',
		brand: {
			type: 'Text',
			value: 'Volkswagen'
		},
		model: {
			type: 'Text',
			value: 'Constellation'
		},
		location: {
			type: 'geo:json',
			value: {
				type: 'Point',
				coordinates: [-23.538274, -46.5302341]
			}
		}
	}
];

let dataArray = farmsArray
	.concat(fieldsArray)
	.concat(productsArray)
	.concat(cropArray)
	.concat(plantationArray)
	.concat(harvestsArray)
	.concat(distributionCentersArray)
	.concat(marketArray)
	.concat(stockArray)
	.concat(trucksArray);

function callback(error, response, body, entity) {
	if (!error && response.statusCode == 201) {
		console.log(`Created "${entity.id}"`);
	} else {
		console.log('Erro: ' + error);
	}
}

for (let i = 0; i < dataArray.length; i++) {
	var dataString = JSON.stringify(dataArray[i]);
	var options = {
		url: 'http://localhost:1026/v2/entities',
		method: 'POST',
		headers: headers,
		body: dataString
	};
	request(options, (error, response, body) => callback(error, response, body, dataArray[i]));
}
